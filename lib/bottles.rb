class Bottles
    def song()
       verses(99, 0)
    end

    def verses(first, last)
        ret = ""
        while first >= last
            ret +=  verse(first)
            first = first - 1
            ret += "\n" if (first == last)
        end
        ret
    end

    def verse(count)
        bottleText = bottleS?(count)
        if (count === 0)
            "No more bottles of beer on the wall, no more bottles of beer.\n" +
             "Go to the store and buy some more, 99 bottles of beer on the wall.\n"
        elsif (count === 1)
            "#{count} #{bottleText} of beer on the wall, " +
            "#{count} #{bottleText} of beer.\n" +
            "Take it down and pass it around, no more bottles of beer on the wall.\n"
        elsif
            bottleTextTwo = bottleS?(count - 1)
            "#{count} #{bottleText} of beer on the wall, " +
            "#{count} #{bottleText} of beer.\n" +
            "Take one down and pass it around, " +
            "#{count-1} #{bottleTextTwo} of beer on the wall.\n"
        end
    end

    def bottleS?(n)
        if (n > 1)
            "bottles"
        else
            "bottle"
        end
    end
end
